<?php

/**
 * @file
 * This file contains the admin callbacks for forms and settings
 */


/**
 * Callback for settings form
 */
function goingup_settings_form($form_state) {
  $form = array();
  $cfg = _goingup_get_cfg();
  
  $form['instructions'] = array(
    '#type' => 'markup',
    '#value' => t('The following information is located on your website setting page for this domain in the !site_key seection.', array('!site_key' => '<strong>Site &amp; API Key</strong>')),
  );
  
  $form[GOINGUP_CFG_SITEID] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#description' => t('This is the site ID located on your website setting page'),
    '#default_value' => $cfg[GOINGUP_CFG_SITEID],
    '#required' => TRUE,
  );
   
  $form[GOINGUP_CFG_APIKEY] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('This is the API key located on your website setting page'),
    '#default_value' => $cfg[GOINGUP_CFG_APIKEY],
    '#required' => TRUE,
  );
   
  $form[GOINGUP_CFG_EXCLUDEPAGES] = array(
    '#type' => 'textarea',
    '#title' => t('Pages to exclude'),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. &lt;front&gt; is the front page."),
    '#default_value' => $cfg[GOINGUP_CFG_EXCLUDEPAGES],
    '#rows' => 10,
  );
  
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save Settings'),
  );

  return $form;
}

/**
 * Callback for form validation
 */
function goingup_settings_form_validate($form, &$form_state) {
  // Nothing to do here since the only two values are required, and FAPI will take care of those
}

/**
 * Callback for form submission
 */
function goingup_settings_form_submit($form, &$form_state) {
  // Copy all fields from form
  $cfg[GOINGUP_CFG_SITEID] = $form_state['values'][GOINGUP_CFG_SITEID];
  $cfg[GOINGUP_CFG_APIKEY] = $form_state['values'][GOINGUP_CFG_APIKEY];
  $cfg[GOINGUP_CFG_EXCLUDEPAGES] = trim($form_state['values'][GOINGUP_CFG_EXCLUDEPAGES]);
  
  // Save 'em off
  _goingup_set_cfg($cfg);
  
  // Let the admin know it was saved
  drupal_set_message(t('Your configuration settings have been saved.'));
}
